import './App.css';
import React, { Component } from "react";
import { render } from "react-dom";

import Categories from'./components/Categories';
import HomePage from './components/HomePage';
import About from './components/About';
   
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom"

function App() {
  return (<Router>
  <Routes>
<Route exact path="/" element={<HomePage/>}/>
<Route exact path="/categories" element={<Categories/>}/>
<Route exact path="/about" element={<About/>}/>
  </Routes>
  </Router>

  );
}

export default App;
