import React from "react";
import "./categories.css";

function Categories() {
  return (
    <div class="bodyCat">
      <div class="container">
        <div class="title">
          <h1>Categories</h1>
        </div>
        <div class="categories">
          <div class="card">
            <div class="face face1">
              <div class="content">
                <img src="https://cdn-icons-png.flaticon.com/512/1584/1584808.png" />
                <a href="#">LESS THAN 10 MNS</a>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="face face1">
              <div class="content">
                <img src="https://www.pngkey.com/png/full/104-1044757_purse-vector-free-flat-design-wallet-icon-png.png" />
                <a href="#">LESS THAN $2</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Categories;
