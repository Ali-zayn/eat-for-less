import React from "react";
import "./About.css";


function About() {

  return (
    <div class="body">
    <section class="section">
      <div class="title">
        <h2>about</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum,
          aperiam!
        </p>
      </div>
      <div class="about-center section-center">
        <article class="about-img">
          <img src="./hero-bcg.jpeg" alt="" />
        </article>
        <article class="about">
            <div class="content" id="vision">
              <h4>vision</h4>
              <p>
                Man bun PBkeytar copper mug prism, hell of helvetica. Synth
                crucifix offal deep v hella biodiesel. Church-key listicle
                polaroid put a bird on it chillwave palo santo enamel pin,
                tattooed meggings franzen la croix cray. Retro yr aesthetic four
                loko tbh helvetica air plant, neutra palo santo tofu mumblecore.
                Hoodie bushwick pour-over jean shorts chartreuse shabby chic.
                Roof party hammock master cleanse pop-up truffaut, bicycle
                rights skateboard affogato readymade sustainable deep v
                live-edge schlitz narwhal.
              </p>
          </div>
        </article>
      </div>
    </section>
    </div>
  );
}

export default About;
