import React from "react";
import "./categories1.scss";


function Categories1() {

  return (
      <div class="bodyCat1">
    <div class="container1">  
  <h1>LESS THAN 10 MNS </h1>  
  <div class="card-container">  
   <div class="card-wrapper">  
    <div class="card1 light">  
     <div class="text-overlay">  
     <img src='https://cdn.loveandlemons.com/wp-content/uploads/2019/09/dinner-ideas-2.jpg' />  
      <h2>Recipe</h2> 
      <h2>time/cost</h2>    
     </div>  
     <div class="purchase-button-container">  
      <h2 class="back-h2">Recipe</h2>  
      <i class="fa-solid fa-person"></i>  
      <div class="purchase-button light">This text is really long and the height of its container is only 100 pixels. Therefore, a scrollbar is added to help the reader to scroll the content. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</div>  
     </div>  
    </div>  
   </div>  
   <div class="card-wrapper">  
    <div class="card1 light">  
     <div class="text-overlay">  
     <img src='https://cdn.loveandlemons.com/wp-content/uploads/2019/09/dinner-ideas-2.jpg' />  
      <h2>Recipe 2</h2> 
      <h2>time/cost</h2>       
     </div>  
     <div class="purchase-button-container">  
      <h2 class="back-h2">Recipe 2</h2>  
      <i class="fa-solid fa-person"></i>  
      <div class="purchase-button light">
        <ul>Ingredients</ul>
        <li>pasta</li>
        <li>tomato</li>
        <li>gerlic</li>
        </div>  
     </div>  
    </div>  
   </div>    
   <div class="card-wrapper">  
    <div class="card1 light">  
     <div class="text-overlay">  
     <img src='https://cdn.loveandlemons.com/wp-content/uploads/2019/09/dinner-ideas-2.jpg' />  
      <h2>Recipe 3</h2>
      <h2>time/cost</h2>        
     </div>  
     <div class="purchase-button-container">  
      <h2 class="back-h2">Recipe 3</h2>  
      <i class="fa-solid fa-person"></i>  
      <div class="purchase-button light">This text is really long and the height of its container is only 100 pixels. Therefore, a scrollbar is added to help the reader to scroll the content. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</div>  
     </div>  
    </div>
    </div> 
  </div>  
 </div> 
   </div>
    );
  }
  
  export default Categories1;