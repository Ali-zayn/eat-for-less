import React from "react";
import "./categories1.css";


function Categories2() {

  return (
      <body>
    <div class="container">  
     <div class="card">  
       <div class="face face1">  
         <div class="content">  
           <img src='https://images.pexels.com/photos/704569/pexels-photo-704569.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' />  
           <h3>Design</h3>  
         </div>  
       </div>  
       <div class="face face2">  
         <div class="content">  
           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum cumque minus iste veritatis provident at.</p>  
             <a href="#">Read More</a>  
         </div>  
       </div>  
     </div>  
     <div class="card">  
       <div class="face face1">  
         <div class="content">  
           <img src="https://github.com/Jhonierpc/WebDevelopment/blob/master/CSS%20Card%20Hover%20Effects/img/code_128.png?raw=true" />  
           <h3>Code</h3>  
         </div>  
       </div>  
       <div class="face face2">  
         <div class="content">  
           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum cumque minus iste veritatis provident at.</p>  
             <a href="#">Read More</a>  
         </div>  
       </div>  
     </div>  
     <div class="card">  
       <div class="face face1">  
         <div class="content">  
           <img src="https://github.com/Jhonierpc/WebDevelopment/blob/master/CSS%20Card%20Hover%20Effects/img/launch_128.png?raw=true"></img>  
           <h3>Launch</h3>  
         </div>  
       </div>  
       <div class="face face2">  
         <div class="content">  
           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum cumque minus iste veritatis provident at.</p>  
             <a href="#">Read More</a>  
         </div>  
       </div>  
     </div>  
   </div> 
   </body>
    );
  }
  
  export default Categories2;